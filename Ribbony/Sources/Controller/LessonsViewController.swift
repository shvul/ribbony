//
//  LessonsViewController.swift
//  Ribbony
//
//  Created by Pavel Serdiukov on 5/9/18.
//  Copyright © 2018 shvul. All rights reserved.
//

import UIKit
import Lottie
import Swinject
import SwinjectStoryboard

class LessonsViewController: UIViewController {
    
    var viewControllerLoader: ViewControllerLoader!

    private lazy var builder: LOTAnimationView = { [unowned self] in
        let builder = LOTAnimationView(name: "Builder")
        builder.loopAnimation = true
        builder.translatesAutoresizingMaskIntoConstraints = false
        builder.contentMode = .scaleAspectFit
        return builder
        }()
    
    @IBAction func goHomeAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func showSidebarAction(_ sender: UIBarButtonItem) {
        viewControllerLoader.showSidebar(for: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(builder)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        builder.play()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        NSLayoutConstraint.activate([
            builder.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            ])
        
        var contraints = [NSLayoutConstraint]()
        contraints.append(NSLayoutConstraint(item: builder,
                                            attribute: .height,
                                            relatedBy: .equal,
                                            toItem: builder,
                                            attribute: .width,
                                            multiplier: 9.0 / 5.0,
                                            constant: 0))
        contraints.append(NSLayoutConstraint(item: builder,
                                             attribute: .bottom,
                                             relatedBy: .equal,
                                             toItem: view,
                                             attribute: .bottom,
                                             multiplier: 0.9,
                                             constant: 0))
        contraints.append(NSLayoutConstraint(item: builder,
                                             attribute: .height,
                                             relatedBy: .equal,
                                             toItem: view,
                                             attribute: .height,
                                             multiplier: 0.3,
                                             constant: 0))
        
        contraints.append(builder.leadingAnchor.constraint(equalTo: view.leadingAnchor))
        NSLayoutConstraint.activate(contraints)
        view.layoutIfNeeded()
    }
}

// MARK: - DI Container
class LessonsViewControllerAssembly: Assembly {
    func assemble(container: Container) {
        container.storyboardInitCompleted(LessonsViewController.self) { (r, c) in
            c.viewControllerLoader = r.resolve(ViewControllerLoader.self)
        }
    }
}
