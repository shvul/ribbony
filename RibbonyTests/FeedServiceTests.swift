//
//  FeedServiceTests.swift
//  RibbonyTests
//
//  Created by Pavel Serdiukov on 3/29/18.
//  Copyright © 2018 shvul. All rights reserved.
//

import XCTest
@testable import Ribbony

class FeedServiceTests: XCTestCase {

    func testExample() {
        let data = try! Data(contentsOf: Bundle.main.url(forResource: "MockFeed", withExtension: "json")!)
        
        let feedService = FeedService(with: S3TransferManager())
        let feed = feedService.parseFeed(from: data)
        print(feed.properties[0].lessons[0].description)
    }
}
