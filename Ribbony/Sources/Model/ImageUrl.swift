//
//  ImageUrl+CoreDataClass.swift
//  Ribbony
//
//  Created by Pavel Serdiukov on 6/26/18.
//  Copyright © 2018 shvul. All rights reserved.
//
//

import Foundation
import CoreData

@objc(ImageUrl)
public class ImageUrl: Url {
    private static let entityName = "ImageUrl"
    
    required convenience public init(from decoder: Decoder) throws {
        guard let contextUserInfoKey = CodingUserInfoKey.context,
            let context = decoder.userInfo[contextUserInfoKey] as? NSManagedObjectContext,
            let entity = NSEntityDescription.entity(forEntityName: ImageUrl.entityName, in: context) else {
                fatalError("Failed to decode ImageUrl! Managed object context is missed or wrong entity name is set.")
        }
        self.init(entity: entity, insertInto: context)
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        scale = try container.decode(Float.self, forKey: .scale)
        url = try container.decode(String.self, forKey: .url)
        md5Hash = try container.decode(String.self, forKey: .md5Hash)
    }
    
}
extension ImageUrl {
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<ImageUrl> {
        return NSFetchRequest<ImageUrl>(entityName: "ImageUrl")
    }
    
    @NSManaged public var scale: Float
    @NSManaged public var property: Property?
    @NSManaged public var lesson: Lesson?
    
}

// MARK: Coding Keys for deserialization
extension ImageUrl {
    private enum CodingKeys: String, CodingKey {
        case scale
        case property
        case lesson
        case url
        case md5Hash
    }
}
