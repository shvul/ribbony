//
//  ResourceUrl+CoreDataClass.swift
//  Ribbony
//
//  Created by Pavel Serdiukov on 6/26/18.
//  Copyright © 2018 shvul. All rights reserved.
//
//

import Foundation
import CoreData
import FirebaseRemoteConfig

@objc(Url)
public class Url: NSManagedObject, Decodable, DownloadableFile {
    private static let entityName = "Url"
    
    lazy var localUrl: URL = {
        let bucket = RemoteConfig.remoteConfig()["bucketName"].stringValue!
        let bundle = Bundle.main.bundleIdentifier!
        let pathComponent = "\(bundle)/feed/" + url.replacingOccurrences(of: bucket, with: "")
        return FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask)[0].appendingPathComponent(pathComponent)
    }()
    
    required convenience public init(from decoder: Decoder) throws {
        guard let contextUserInfoKey = CodingUserInfoKey.context,
            let context = decoder.userInfo[contextUserInfoKey] as? NSManagedObjectContext,
            let entity = NSEntityDescription.entity(forEntityName: Url.entityName, in: context) else {
                fatalError("Failed to decode Url! Managed object context is missed or wrong entity name is set.")
        }
        self.init(entity: entity, insertInto: context)
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        url = try container.decode(String.self, forKey: .url)
        md5Hash = try container.decode(String.self, forKey: .md5Hash)
    }
}

extension Url {
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<Url> {
        return NSFetchRequest<Url>(entityName: "Url")
    }
    
    @NSManaged public var url: String
    @NSManaged public var md5Hash: String
}

// MARK: Coding Keys for deserialization
extension Url {
    private enum CodingKeys: String, CodingKey {
        case url
        case md5Hash
    }
}
