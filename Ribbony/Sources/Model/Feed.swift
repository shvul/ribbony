//
//  Feed+CoreDataClass.swift
//  Ribbony
//
//  Created by Pavel Serdiukov on 6/26/18.
//  Copyright © 2018 shvul. All rights reserved.
//
//

import Foundation
import CoreData

@objc(Feed)
public class Feed: NSManagedObject, Decodable {
    
    static let entityName = "Feed"
    
    required convenience public init(from decoder: Decoder) throws {
        guard let contextUserInfoKey = CodingUserInfoKey.context,
            let context = decoder.userInfo[contextUserInfoKey] as? NSManagedObjectContext,
            let entity = NSEntityDescription.entity(forEntityName: Feed.entityName, in: context) else {
            fatalError("Failed to decode Feed! Managed object context is missed or wrong entity name is set.")
        }
        self.init(entity: entity, insertInto: context)
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        date = try container.decode(Date.self, forKey: .date)
        language = try container.decode(String.self, forKey: .language)
        version = try container.decode(String.self, forKey: .version)
        models = Set(try container.decode([Model].self, forKey: .models))
        properties = Set(try container.decode([Property].self, forKey: .properties))
    }
}
extension Feed {
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<Feed> {
        return NSFetchRequest<Feed>(entityName: entityName)
    }
    
    @NSManaged public var date: Date
    @NSManaged public var language: String
    @NSManaged public var version: String
    @NSManaged public var models: Set<Model>
    @NSManaged public var properties: Set<Property>
    
}

// MARK: Generated accessors for models
extension Feed {
    
    @objc(addModelsObject:)
    @NSManaged public func addToModels(_ value: Model)
    
    @objc(removeModelsObject:)
    @NSManaged public func removeFromModels(_ value: Model)
    
    @objc(addModels:)
    @NSManaged public func addToModels(_ values: Set<Model>)
    
    @objc(removeModels:)
    @NSManaged public func removeFromModels(_ values: Set<Model>)
    
}

// MARK: Generated accessors for properties
extension Feed {
    
    @objc(addPropertiesObject:)
    @NSManaged public func addToProperties(_ value: Property)
    
    @objc(removePropertiesObject:)
    @NSManaged public func removeFromProperties(_ value: Property)
    
    @objc(addProperties:)
    @NSManaged public func addToProperties(_ values: Set<Property>)
    
    @objc(removeProperties:)
    @NSManaged public func removeFromProperties(_ values: Set<Property>)
    
}

// MARK: Coding Keys for deserialization
extension Feed {
    enum CodingKeys: String, CodingKey {
        case date
        case language
        case version
        case models
        case properties
    }
}

extension CodingUserInfoKey {
    static let context = CodingUserInfoKey(rawValue: "context")
}
