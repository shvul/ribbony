//
//  VirtualObjectARView.swift
//  Ribbony
//
//  Created by Pavel Serdiukov on 5/14/18.
//  Copyright © 2018 shvul. All rights reserved.
//

import Foundation
import ARKit

class VirtualObjectARView: ARSCNView {
    
    // MARK: Position Testing
    
    /// Hit tests against the `sceneView` to find an object at the provided point.
    func virtualObject(at point: CGPoint) -> VirtualObject? {
        let hitTestOptions: [SCNHitTestOption: Any] = [.boundingBoxOnly: true]
        let hitTestResults = hitTest(point, options: hitTestOptions)
        
        return hitTestResults.lazy.compactMap { result in
            return VirtualObject.existingObjectContainingNode(result.node)
            }.first
    }
    
    func smartHitTest(_ point: CGPoint,
                      infinitePlane: Bool = false,
                      objectPosition: float3? = nil,
                      allowedAlignments: [ARPlaneAnchor.Alignment] = [.horizontal]) -> ARHitTestResult? {

        let results = hitTest(point, types: [.estimatedHorizontalPlane])
        if infinitePlane {
        
            let infinitePlaneResults = hitTest(point, types: .existingPlane)
            
            for infinitePlaneResult in infinitePlaneResults {
                if let planeAnchor = infinitePlaneResult.anchor as? ARPlaneAnchor, allowedAlignments.contains(planeAnchor.alignment) {
                   
                        if let objectY = objectPosition?.y {
                            let planeY = infinitePlaneResult.worldTransform.translation.y
                            if objectY > planeY - 0.05 && objectY < planeY + 0.05 {
                                return infinitePlaneResult
                            }
                        } else {
                            return infinitePlaneResult
                        }
                    
                }
            }
        }
    
        let hResult = results.first(where: { $0.type == .estimatedHorizontalPlane })
        if allowedAlignments.contains(.horizontal) {
            return hResult
        } else {
            return nil
        }
    }
    
    // - MARK: Object anchors
    func addOrUpdateAnchor(for object: VirtualObject) {
        if let anchor = object.anchor {
            session.remove(anchor: anchor)
        }
        
        let newAnchor = ARAnchor(transform: object.simdWorldTransform)
        object.anchor = newAnchor
        session.add(anchor: newAnchor)
    }
}

extension SCNView {
    func unprojectPoint(_ point: float3) -> float3 {
        return float3(unprojectPoint(SCNVector3(point)))
    }
}
