//
//  ActivityIndicator.swift
//  Ribbony
//
//  Created by Pavel Serdiukov on 1/5/18.
//  Copyright © 2018 shvul. All rights reserved.
//

import UIKit
import Lottie

protocol ActivityDelegate: class {
    func animationDidFinished(animation: ActivityAnimation)
}

enum ActivityAnimation {
    case spinner
    case success
    case failure
}

protocol AnyView: class {
    var view: UIView { get }
}
protocol ActivityIndicator: AnyView where Self: UIView {
    var delegate: ActivityDelegate? { get set }
    
    func play(animation: ActivityAnimation)
    
    func stop()
}

class SphereActivityIndicator: UIView {
    weak var delegate: ActivityDelegate?
    var view: UIView {
        get {
            return self
        }
    }
    
    private var nextAnimation: ActivityAnimation?
    private lazy var activityAnimation: LOTAnimationView = { [unowned self] in
        let animation = LOTAnimationView(name: "Spinner")
        animation.contentMode = .scaleAspectFit
        animation.animationSpeed = 0.8
        animation.translatesAutoresizingMaskIntoConstraints = false
        addSubview(animation)
        return animation
        }()
    private lazy var complitionBlock: LOTAnimationCompletionBlock = { [weak self] (completed) in
        if (completed) {
            if let this = self, let animation = self!.nextAnimation {
                this.start(animation: animation)
                this.nextAnimation = nil
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        activityAnimation.frame = frame
        backgroundColor = .clear
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        activityAnimation.frame = frame
    }
}

// MARK: - ActivityIndicator
extension SphereActivityIndicator: ActivityIndicator {
    
    func stop() {
        activityAnimation.loopAnimation = false
    }
    
    func play(animation: ActivityAnimation) {
        stop()
        if activityAnimation.isAnimationPlaying {
            nextAnimation = animation
        } else {
            start(animation: animation)
        }
    }
    
    private func start(animation: ActivityAnimation) {
        activityAnimation.loopAnimation = shouldAnimationBeLooped(animation)
        switch animation {
        case .spinner:
            playSpinnerAnimation()
        case .success:
            playSuccessAnimation()
        case .failure:
            playFailureAnimation()
        }
    }
    
    private func shouldAnimationBeLooped(_ animation: ActivityAnimation) -> Bool {
        return animation == .spinner
    }
    
    struct LiquidColor {
        private init(){}
        
        static let success = UIColor(red: 128.0/255.0, green:  178.0/255.0, blue:  98.0/255.0, alpha: 1.0)
        static let failure = UIColor(red: 205/255.0, green:  75/255.0, blue:  75/255.0, alpha: 1.0)
    }
    
    func playSpinnerAnimation() {
        activityAnimation.play(toFrame: 120, withCompletion: complitionBlock)
    }
    
    func playSuccessAnimation() {
        setLiguidColor(LiquidColor.success)
        activityAnimation.play(fromFrame: 120, toFrame: 180, withCompletion: complitionBlock)
    }
    
    func playFailureAnimation() {
        setLiguidColor(LiquidColor.failure)
        activityAnimation.play(fromFrame: 120, toFrame: 150) { [weak self] (completed) in
            if (completed) {
                self?.activityAnimation.play(fromFrame: 180, toFrame: 200, withCompletion: self?.complitionBlock)
            }
        }
    }
    
    func setLiguidColor(_ color: UIColor) {
        activityAnimation.setValue(color, forKeypath: "Liquid.Ellipse 1.Fill 1.Color", atFrame: 0)
        activityAnimation.setValue(color, forKeypath: "Liquid.Ellipse 1.Stroke 1.Color", atFrame: 0)
    }
}
// MARK: - Lifecycle
extension SphereActivityIndicator {
    public override func layoutSubviews() {
        NSLayoutConstraint.activate([
            activityAnimation.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            activityAnimation.topAnchor.constraint(equalTo: self.topAnchor),
            activityAnimation.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            activityAnimation.trailingAnchor.constraint(equalTo: self.trailingAnchor)
            ])
    }
}

