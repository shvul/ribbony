//
//  TransitionNotifications.swift
//  Ribbony
//
//  Created by Pavel Serdiukov on 1/2/18.
//  Copyright © 2018 shvul. All rights reserved.
//

import Foundation

struct TransitionNotofications {
    
    private init() {}
    
    static let introAnimationFinished = NSNotification.Name("PSIntroAnimationFinishedTransitionNotification")
}
