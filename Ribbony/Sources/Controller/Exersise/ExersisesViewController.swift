//
//  ExersisesViewController.swift
//  Ribbony
//
//  Created by Pavel Serdiukov on 6/15/18.
//  Copyright © 2018 shvul. All rights reserved.
//

import UIKit
import Swinject
import SwinjectStoryboard

private let reuseIdentifier = "ExersiseCell"
private let segueIdentifier = "exersiseNavigation"

class ExersisesViewController: UICollectionViewController {

    var viewControllerLoader: ViewControllerLoader!
    
    @IBAction func showSidebarAction(_ sender: UIBarButtonItem) {
        viewControllerLoader.showSidebar(for: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let cellNib = UINib(nibName: "ExersiseCell", bundle: nil)
        self.collectionView!.register(cellNib, forCellWithReuseIdentifier: reuseIdentifier)
    }

    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == segueIdentifier {
            let cell = collectionView(collectionView!, cellForItemAt: sender as! IndexPath) as! ExersiseCell
            let vc = segue.destination as! ExersiseViewController
            vc.title = cell.title.text
        }
    }

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
    
        if let exersiseCell = cell as? ExersiseCell {
            exersiseCell.title.text = "Beat"
            exersiseCell.thumbnail.image = UIImage(named: "ex_test_img")
        }
    
        return cell
    }

    // MARK: UICollectionViewDelegate
    
    override func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
        guard let cell  = collectionView.cellForItem(at: indexPath) else { return }
        UIView.animate(withDuration: 0.1) {
            cell.transform = CGAffineTransform(scaleX: 0.95, y: 0.95)
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
        guard let cell  = collectionView.cellForItem(at: indexPath) else { return }
        UIView.animateKeyframes(withDuration: 0.5, delay: 0, options: [.calculationModeCubic], animations: {
            UIView.addKeyframe(withRelativeStartTime: 0.0, relativeDuration: 1.0/5.0, animations: {
                cell.transform = CGAffineTransform.identity.scaledBy(x: 1.02, y: 1.02)
            })
            UIView.addKeyframe(withRelativeStartTime: 1.0/5.0, relativeDuration: 2.0/5.0, animations: {
                cell.transform = CGAffineTransform.identity.scaledBy(x: 0.98, y: 0.98)
            })
            UIView.addKeyframe(withRelativeStartTime: 4.0/5.0, relativeDuration: 2.0/5.0, animations: {
                cell.transform = CGAffineTransform.identity
            })
        })
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: segueIdentifier, sender: indexPath)
    }

}

// MARK: UICollectionViewDelegateFlowLayout

extension ExersisesViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenWidth = UIScreen.main.bounds.width
        let cellWidth = screenWidth - 20
        let cellHeight = cellWidth / 16 * 9
        return CGSize(width: cellWidth, height: cellHeight)
    }
}

// MARK: - DI Container
class ExersisesViewControllerAssembly: Assembly {
    func assemble(container: Container) {
        container.storyboardInitCompleted(ExersisesViewController.self) { (r, c) in
            c.viewControllerLoader = r.resolve(ViewControllerLoader.self)
        }
    }
}

