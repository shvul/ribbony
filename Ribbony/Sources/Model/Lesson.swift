//
//  Lesson+CoreDataClass.swift
//  Ribbony
//
//  Created by Pavel Serdiukov on 6/26/18.
//  Copyright © 2018 shvul. All rights reserved.
//
//

import Foundation
import CoreData

@objc(Lesson)
public class Lesson: NSManagedObject, Decodable {

    private static let entityName = "Lesson"
    
    required convenience public init(from decoder: Decoder) throws {
        guard let contextUserInfoKey = CodingUserInfoKey.context,
            let context = decoder.userInfo[contextUserInfoKey] as? NSManagedObjectContext,
            let entity = NSEntityDescription.entity(forEntityName: Lesson.entityName, in: context) else {
                fatalError("Failed to decode Lesson! Managed object context is missed or wrong entity name is set.")
        }
        self.init(entity: entity, insertInto: context)
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        authRequired = try container.decode(Bool.self, forKey: .authRequired)
        desc = try container.decode(String.self, forKey: .description)
        id = try container.decode(String.self, forKey: .id)
        title = try container.decode(String.self, forKey: .title)
        animations = Set(try container.decode([Animation].self, forKey: .animations))
        exerciseImage = Set(try container.decode([ImageUrl].self, forKey: .exerciseImage))
    }
}

extension Lesson {
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<Lesson> {
        return NSFetchRequest<Lesson>(entityName: "Lesson")
    }
    
    @NSManaged public var authRequired: Bool
    @NSManaged public var desc: String
    @NSManaged public var id: String
    @NSManaged public var title: String
    @NSManaged public var animations: Set<Animation>
    @NSManaged public var exerciseImage: Set<ImageUrl>
    @NSManaged public var property: Property?
    
}

// MARK: Generated accessors for animations
extension Lesson {
    
    @objc(addAnimationsObject:)
    @NSManaged public func addToAnimations(_ value: Animation)
    
    @objc(removeAnimationsObject:)
    @NSManaged public func removeFromAnimations(_ value: Animation)
    
    @objc(addAnimations:)
    @NSManaged public func addToAnimations(_ values: Set<Animation>)
    
    @objc(removeAnimations:)
    @NSManaged public func removeFromAnimations(_ values: Set<Animation>)
    
}

// MARK: Generated accessors for exerciseImage
extension Lesson {
    
    @objc(addExerciseImageObject:)
    @NSManaged public func addToExerciseImage(_ value: ImageUrl)
    
    @objc(removeExerciseImageObject:)
    @NSManaged public func removeFromExerciseImage(_ value: ImageUrl)
    
    @objc(addExerciseImage:)
    @NSManaged public func addToExerciseImage(_ values: Set<ImageUrl>)
    
    @objc(removeExerciseImage:)
    @NSManaged public func removeFromExerciseImage(_ values: Set<ImageUrl>)
    
}

// MARK: Coding Keys for deserialization
extension Lesson {
    private enum CodingKeys: String, CodingKey {
        case authRequired
        case description
        case id
        case title
        case animations
        case exerciseImage
        case property
    }
}
