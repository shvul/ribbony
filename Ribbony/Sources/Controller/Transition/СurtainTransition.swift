//
//  СurtainTransition.swift
//  Ribbony
//
//  Created by Pavel Serdiukov on 1/2/18.
//  Copyright © 2018 shvul. All rights reserved.
//

import UIKit

class CurtainTransition: NSObject, UIViewControllerAnimatedTransitioning {
    
    private var animationView: UIView?
    private var fromVC: UIViewController?
    private var context: UIViewControllerContextTransitioning?
    
    // MARK: - UIViewControllerAnimatedTransitioning
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 1.0
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard let toVC = transitionContext.viewController(forKey: .to),
        let fromVC = transitionContext.viewController(forKey: .from) else {
                transitionContext.completeTransition(false)
                return
        }
        context = transitionContext
        self.fromVC = fromVC
        let containerView = transitionContext.containerView
        containerView.addSubview(toVC.view)

        let duration = transitionDuration(using: transitionContext)
        animationView = toVC.view
        animate(controller: toVC, withDuration: duration)
    }
    
    // MARK: - Animations
    
    private func animate(controller: UIViewController, withDuration duration:TimeInterval) {
        let view = controller.view!
        let initialBounds = view.bounds
        let initialPath = UIBezierPath(rect: initialBounds)
        initialPath.append(UIBezierPath(rect: initialBounds))
        
        let mask = CAShapeLayer()
        mask.fillRule = kCAFillRuleEvenOdd
        mask.path = initialPath.cgPath
        view.layer.mask = mask
        
        let finalRect =  CGRect(withCenter: initialBounds.center, size: CGSize(width: initialBounds.width, height: 0.00001))
        let finalPath = UIBezierPath(rect: finalRect)
        finalPath.append(UIBezierPath(rect: initialBounds))
        
        CATransaction.begin()
        let animation = CABasicAnimation(keyPath: "path")
        animation.fromValue = initialPath.cgPath
        animation.toValue = finalPath.cgPath
        animation.duration = duration
        animation.timingFunction = CAMediaTimingFunction(controlPoints: 1.0, 0.55, 0.42, 1.0)
        animation.fillMode = kCAFillModeForwards
        animation.isRemovedOnCompletion = false
        CATransaction.setCompletionBlock {
            self.context?.completeTransition(true)
            self.fromVC?.view.removeFromSuperview()
            NotificationCenter.default.post(name: TransitionNotofications.introAnimationFinished, object: nil)
        }
        mask.add(animation, forKey: "scale")
        CATransaction.commit()
    }
}
