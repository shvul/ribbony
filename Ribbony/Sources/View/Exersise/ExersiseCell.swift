//
//  ExersiseCell.swift
//  Ribbony
//
//  Created by Pavel Serdiukov on 6/14/18.
//  Copyright © 2018 shvul. All rights reserved.
//

import UIKit
import SDWebImage
import FirebaseStorage
import FirebaseStorageUI

class ExersiseCell: UICollectionViewCell {
    @IBOutlet weak var thumbnail: UIImageView!
    @IBOutlet weak var title: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.cornerRadius = 5
    }

}
