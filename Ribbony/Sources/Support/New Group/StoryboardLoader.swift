//
//  StoryboardLoader.swift
//  Ribbony
//
//  Created by Pavel Serdiukov on 7/6/18.
//  Copyright © 2018 shvul. All rights reserved.
//

import UIKit
import Swinject

protocol ViewControllerLoader {
    func showSidebar(for controller: UIViewController)
}
class StoryboardLoader: ViewControllerLoader {
    func showSidebar(for controller: UIViewController) {
        let storyboardName = "Sidebar"
        let bundle = Bundle.main
        let storyboard = UIStoryboard(name: storyboardName, bundle: bundle)
        let sidebar =  storyboard.instantiateViewController(withIdentifier: "Sidebar")
        sidebar.modalPresentationStyle = .overCurrentContext
        sidebar.modalTransitionStyle = .crossDissolve
        DispatchQueue.main.async {
            controller.present(sidebar, animated: false, completion: nil)
        }
    }
}

// MARK: - DI Container
class ViewControllerLoaderAssembly: Assembly {
    func assemble(container: Container) {
        container.register(ViewControllerLoader.self) { r in
            return StoryboardLoader()
            }.inObjectScope(.container)
    }
}
