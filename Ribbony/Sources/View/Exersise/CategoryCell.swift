//
//  CategoryCellCollectionViewCell.swift
//  Ribbony
//
//  Created by Pavel Serdiukov on 6/12/18.
//  Copyright © 2018 shvul. All rights reserved.
//

import UIKit

class CategoryCell: UICollectionViewCell {
    @IBOutlet weak var categoryImage: UIImageView!
    @IBOutlet weak var title: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.cornerRadius = 5
    }

}
