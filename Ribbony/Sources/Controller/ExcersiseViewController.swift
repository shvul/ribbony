//
//  ExcersiseViewController.swift
//  Ribbony
//
//  Created by Pavel Serdiukov on 4/19/18.
//  Copyright © 2018 shvul. All rights reserved.
//

import UIKit
import SceneKit
import ZCAnimatedLabel

class ExersiseViewController: UIViewController {
    @IBOutlet weak var sceneWrapperView: UIView!
    @IBOutlet weak var descriptionText: UITextView!
    
    private lazy var sceneView: SCNView = { [unowned self] in
        let sceneView = SCNView()
        let scene = SCNScene(named: "Art.scnassets/girl.scn")!
        sceneView.scene = scene
        sceneView.allowsCameraControl = true
        sceneView.backgroundColor = UIColor.white
        sceneView.contentMode = .scaleAspectFit
        return sceneView
        }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.async {
            self.view.addSubview(self.sceneView)
            self.setupSceneViewContraints()
            self.sceneView.layer.add(self.getOpacityAniamtion(), forKey: "opacityAnimation")
            self.showDescription()
        }
    }
    
    private func setupSceneViewContraints() {
        sceneView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            sceneView.leadingAnchor.constraint(equalTo: sceneWrapperView.leadingAnchor),
            sceneView.topAnchor.constraint(equalTo: sceneWrapperView.topAnchor),
            sceneView.bottomAnchor.constraint(equalTo: sceneWrapperView.bottomAnchor),
            sceneView.trailingAnchor.constraint(equalTo: sceneWrapperView.trailingAnchor)
            ])
        view.layoutIfNeeded()
    }
    
    private func showDescription() {
        
        let positionAnimation = CABasicAnimation(keyPath: "position.y")
        let initialPositionY = descriptionText.layer.position.y
        positionAnimation.fromValue = initialPositionY * 1.03
        positionAnimation.toValue = initialPositionY
        positionAnimation.duration = 0.7
        descriptionText.layer.add(positionAnimation, forKey: "positionAnimation")
        descriptionText.layer.add(getOpacityAniamtion(), forKey: "opacityAnimation")
        
        self.descriptionText.isHidden = false;
    }
    
    private func getOpacityAniamtion() -> CABasicAnimation {
        let opacityAnimation = CABasicAnimation(keyPath: "opacity")
        opacityAnimation.fromValue = 0
        opacityAnimation.toValue = 1
        opacityAnimation.duration = 0.7
        return opacityAnimation
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
//    let jumpAnimation = SCNAnimationPlayer.loadAnimation(fromSceneNamed: "jump.scn")
//    jumpAnimation.stop() // stop it for now so that we can use it later when it's appropriate
//    model.addAnimationPlayer(jumpAnimation, forKey: "jump")

}

extension SCNAnimation {
    static func fromFile(named name: String, inDirectory: String ) -> SCNAnimation? {
        let animScene = SCNScene(named: name, inDirectory: inDirectory)
        var animation: SCNAnimation?
        animScene?.rootNode.enumerateChildNodes({ (child, stop) in
            if !child.animationKeys.isEmpty {
                let player = child.animationPlayer(forKey: child.animationKeys[0])
                animation = player?.animation
                stop.initialize(to: true)
            }
        })
        
        animation?.keyPath = name
        
        return animation
    }
}
