//
//  Queue.swift
//  Ribbony
//
//  Created by Pavel Serdiukov on 1/6/18.
//  Copyright © 2018 shvul. All rights reserved.
//

import Foundation

struct Queue<T> {
    private var array = [T?]()
    private var head = 0
    
    var isEmpty: Bool {
        return count == 0
    }
    
    var count: Int {
        return array.count - head
    }
    
    mutating func enqueue(_ element: T) {
        array.append(element)
    }
    
    mutating func dequeue() -> T? {
        guard head < array.count, let element = array[head] else { return nil }
        
        array[head] = nil
        head += 1
        
        let percentage = Double(head)/Double(array.count)
        if array.count > 50 && percentage > 0.25 {
            array.removeFirst(head)
            head = 0
        }
        
        return element
    }
    
    mutating func clean() {
        head = 0
        array.removeAll()
    }
    
    var front: T? {
        if isEmpty {
            return nil
        } else {
            return array[head]
        }
    }
}
