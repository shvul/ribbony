//
//  Preloader.swift
//  Ribbony
//
//  Created by Pavel Serdiukov on 3/9/18.
//  Copyright © 2018 shvul. All rights reserved.
//

import UIKit
import Lottie
protocol LoaderDelegate: class {
    func loadingFinished()
}
protocol Preloader: AnyView where Self: UIView {
    var delegate: LoaderDelegate? { get set }
    
    func updateProgress(toPercent percent: CGFloat)
    func complete()
    func reset()
}
class LiquidPreloader: UIView {

    @IBOutlet weak var logoOutline: UIImageView!
    private static let nibName = "Preloader"
    private var isAnimating = false
    private let duration = 0.9
    private var contentView: UIView?
    private var liquidTopConstraint: NSLayoutConstraint?
    private lazy var flowAnimationQueue = Queue<CGFloat>()
    weak var delegate: LoaderDelegate?
    var currentPercent: CGFloat = 0.0
    var view: UIView {
        get {
            return self
        }
    }
    
    private lazy var liquid: LOTAnimationView = { [unowned self] in
        let animation = LOTAnimationView(name: "PreloadLiquid")
        animation.contentMode = .scaleToFill
        animation.translatesAutoresizingMaskIntoConstraints = false
        contentView?.addSubview(animation)
        contentView?.sendSubview(toBack: animation)
        animation.loopAnimation = true
        animation.animationSpeed = 2.0
        animation.play()
        return animation
        }()
    
    private var  circleBottom: CGFloat {
        return logoOutline.bounds.height / 3 * 2 - 1 // 1 - multiplier fault
    }
    
    private var  circleTop: CGFloat {
        return logoOutline.bounds.height / 3.1 - 1 // 1 - multiplier fault
    }
    
    private var  circleHeight: CGFloat {
        return circleBottom - circleTop
    }
    
    
    // MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        xibSetup()
    }
    
    func xibSetup() {
        guard let view = loadViewFromNib() else { return }
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
        contentView = view
    }
    
    func loadViewFromNib() -> UIView? {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: LiquidPreloader.nibName, bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as? UIView
    }
    
    override func layoutSubviews() {
        setupLiquidConstraints()
    }
    
    private func setupLiquidConstraints() {
        liquid.widthAnchor.constraint(equalTo: logoOutline.widthAnchor).isActive = true
        liquid.widthAnchor.constraint(equalTo: liquid.heightAnchor, multiplier: 9.0/10.0).isActive = true
        liquid.centerXAnchor.constraint(equalTo: logoOutline.centerXAnchor).isActive = true
        liquidTopConstraint = liquid.topAnchor.constraint(equalTo: logoOutline.topAnchor, constant: circleBottom)
        liquidTopConstraint?.isActive = true
    }
}

// MARK: - Preloader
extension LiquidPreloader: Preloader {
    
    func updateProgress(toPercent percent: CGFloat) {
        layoutIfNeeded()
        if isAnimating {
            flowAnimationQueue.enqueue(percent)
            return
        }
        isAnimating = true
        self.currentPercent = percent
        
        UIView.animate(withDuration: duration, animations: {
            self.liquidTopConstraint?.constant = self.circleTop + (1.0 - percent) * self.circleHeight
            self.layoutIfNeeded()
        }, completion: { finished in
            self.isAnimating = false
            self.checkQueueForAnimation()
        })
    }
    
    private func checkQueueForAnimation() {
        if !isAnimating && flowAnimationQueue.count > 0 {
            updateProgress(toPercent: flowAnimationQueue.dequeue()!)
        } else if !isAnimating && currentPercent >= 1.0 {
            delegate?.loadingFinished()
        }
    }
    
    func complete() {
        flowAnimationQueue.clean()
        updateProgress(toPercent: 1.0)
    }
    
    func reset() {
        flowAnimationQueue.clean()
        updateProgress(toPercent: 0.0)
    }
}
