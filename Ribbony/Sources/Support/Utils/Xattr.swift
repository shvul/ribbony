//
//  Xattr.swift
//  Ribbony
//
//  Created by Pavel Serdiukov on 6/21/18.
//  Copyright © 2018 shvul. All rights reserved.
//

import Foundation

struct Xattr {
    
    struct Error: Swift.Error {
        
        let localizedDescription = String(utf8String: strerror(errno))
    }
    
    static func set(named name: String, data: Data, atPath path: String) throws {
        
        if setxattr(path, name, (data as NSData).bytes, data.count, 0, 0) == -1 { throw Error() }
    }
    
    static func remove(named name: String, atPath path: String) throws {
        
        if removexattr(path, name, 0) == -1 { throw Error() }
    }
    
    static func dataFor(named name: String, atPath path: String) throws -> Data {
        
        let bufLength = getxattr(path, name, nil, 0, 0, 0)
        
        guard bufLength != -1, let buf = malloc(bufLength), getxattr(path, name, buf, bufLength, 0, 0) != -1 else { throw Error() }
        
        return Data(bytes: buf, count: bufLength)
    }
    
    static func names(atPath path: String) throws -> [String]? {
        
        let bufLength = listxattr(path, nil, 0, 0)
        
        guard bufLength != -1 else { throw Error() }
        
        let buf = UnsafeMutablePointer<Int8>.allocate(capacity: bufLength)
        
        guard listxattr(path, buf, bufLength, 0) != -1 else { throw Error() }
        
        var names = NSString(bytes: buf, length: bufLength, encoding: String.Encoding.utf8.rawValue)?.components(separatedBy: "\0")
        
        names?.removeLast()
        
        return names
    }
}
