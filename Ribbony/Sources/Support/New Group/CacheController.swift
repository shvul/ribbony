//
//  FeedService.swift
//  Ribbony
//
//  Created by Pavel Serdiukov on 1/1/18.
//  Copyright © 2018 shvul. All rights reserved.
//

import Foundation
import Swinject

protocol Cachable {
    var md5Hash: String { get set }
    var localUrl: URL { get set }
}
protocol CacheController {
    func cache(object: Cachable)
    func cleanTrash()
    func clean()
    func isObjectCached(_ object: Cachable) -> Bool
}
class FileCacheController: CacheController {
    
    let dataManager: DataBaseManager
    
    lazy var cacheUrl: URL = {
        let bundle = Bundle.main.bundleIdentifier!
        let cacheDir = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask)[0]
        return cacheDir.appendingPathComponent(bundle).appendingPathComponent("feed")
    }()
    
    init(dataManager: DataBaseManager) {
        self.dataManager = dataManager
    }
    
    func cache(object: Cachable) {
        guard let hashData = object.md5Hash.data(using: .utf8) else { return }
        do {
            try Xattr.set(named: "md5Hash", data: hashData, atPath: object.localUrl.path)
        } catch {
            print("Error while caching object(\(object.localUrl) : \(error.localizedDescription)")
        }
    }
    
    func cleanTrash() {
        let storageUrls = dataManager.getUrls(with: UIConstants.scale).map { $0.localUrl.absoluteURL }
        
        let fileManager = FileManager.default
        guard fileManager.fileExists(atPath: cacheUrl.path) else { return }
        do {
            let keys: [URLResourceKey] = [.isDirectoryKey, .isHiddenKey]
            let enumerator = fileManager.enumerator(at: cacheUrl, includingPropertiesForKeys: keys)
            
            while let element = enumerator?.nextObject() as? URL {
                let resourceValues = try element.resourceValues(forKeys: Set(keys))
                if resourceValues.isHidden! || resourceValues.isDirectory! {
                    continue
                }
                if storageUrls.contains(element.absoluteURL) {
                    continue
                }
                try fileManager.removeItem(at: element)
            }
        } catch {
            print("Error while cleaning cache: \(error.localizedDescription)")
        }
    }
    
    func clean() {
        guard FileManager.default.fileExists(atPath: cacheUrl.path) else { return }
        do {
            try FileManager.default.removeItem(at: cacheUrl)
        } catch {
            print("Error while cleaning cache: \(error.localizedDescription)")
        }
    }
    
    func isObjectCached(_ object: Cachable) -> Bool {
        guard let attrData = try? Xattr.dataFor(named: "md5Hash", atPath: object.localUrl.path) else {
            return false
        }
        guard let cachedHash = String(data: attrData, encoding: .utf8) else {
            return false
        }
        return object.md5Hash == cachedHash
    }
}

// MARK: - DI Container
class CacheControllerAssembly: Assembly {
    func assemble(container: Container) {
        container.register(CacheController.self) { r in
            return FileCacheController(dataManager: r.resolve(DataBaseManager.self)!)
        }.inObjectScope(.container)
    }
}


