//
//  Property+CoreDataClass.swift
//  Ribbony
//
//  Created by Pavel Serdiukov on 6/26/18.
//  Copyright © 2018 shvul. All rights reserved.
//
//

import Foundation
import CoreData

@objc(Property)
public class Property: NSManagedObject, Decodable {

    private static let entityName = "Property"
    
    required convenience public init(from decoder: Decoder) throws {
        guard let contextUserInfoKey = CodingUserInfoKey.context,
            let context = decoder.userInfo[contextUserInfoKey] as? NSManagedObjectContext,
            let entity = NSEntityDescription.entity(forEntityName: Property.entityName, in: context) else {
                fatalError("Failed to decode Property! Managed object context is missed or wrong entity name is set.")
        }
        self.init(entity: entity, insertInto: context)
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
    
        id = try container.decode(String.self, forKey: .id)
        title = try container.decode(String.self, forKey: .title)
        lessons = Set(try container.decode([Lesson].self, forKey: .lessons))
        propertyImage = Set(try container.decode([ImageUrl].self, forKey: .propertyImage))
    }
}
extension Property {
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<Property> {
        return NSFetchRequest<Property>(entityName: "Property")
    }
    
    @NSManaged public var id: String
    @NSManaged public var title: String
    @NSManaged public var lessons: Set<Lesson>
    @NSManaged public var propertyImage: Set<ImageUrl>
    
}

// MARK: Generated accessors for lessons
extension Property {
    
    @objc(addLessonsObject:)
    @NSManaged public func addToLessons(_ value: Lesson)
    
    @objc(removeLessonsObject:)
    @NSManaged public func removeFromLessons(_ value: Lesson)
    
    @objc(addLessons:)
    @NSManaged public func addToLessons(_ values: Set<Lesson>)
    
    @objc(removeLessons:)
    @NSManaged public func removeFromLessons(_ values: Set<Lesson>)
    
}

// MARK: Generated accessors for propertyImage
extension Property {
    
    @objc(addPropertyImageObject:)
    @NSManaged public func addToPropertyImage(_ value: ImageUrl)
    
    @objc(removePropertyImageObject:)
    @NSManaged public func removeFromPropertyImage(_ value: ImageUrl)
    
    @objc(addPropertyImage:)
    @NSManaged public func addToPropertyImage(_ values: Set<ImageUrl>)
    
    @objc(removePropertyImage:)
    @NSManaged public func removeFromPropertyImage(_ values: Set<ImageUrl>)
    
}

// MARK: Coding Keys for deserialization
extension Property {
    private enum CodingKeys: String, CodingKey {
        case id
        case title
        case lessons
        case propertyImage
    }
}
