//
//  ViewController.swift
//  Ribbony
//
//  Created by Pavel Serdiukov on 12/16/17.
//  Copyright © 2017 shvul. All rights reserved.
//

import UIKit
import Lottie

class HomeViewController: UIViewController {

    @IBOutlet weak var buttonsStackView: UIStackView!
    weak var logoAnimation: LOTAnimationView?
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addLogoAnimation()
        buttonsStackView.isHidden = true
        NotificationCenter.default.addObserver(self, selector: #selector(introAnimationFinished), name: TransitionNotofications.introAnimationFinished, object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setupConstrains()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.logoAnimation?.setProgressWithFrame(120)
        }
        
    }
    
    // MARK: - Constraints
    
    private func setupConstrains() {
        var allConstraints: [NSLayoutConstraint] = []
        buttonsStackView.translatesAutoresizingMaskIntoConstraints = false
        let views: [String: Any] = ["logo": logoAnimation!, "buttons":buttonsStackView]
        
        buttonsStackView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    
        allConstraints += NSLayoutConstraint.constraints(withVisualFormat: "V:[logo]-30-[buttons]",
                                                         options: .alignAllCenterX,
                                                         metrics: nil,
                                                         views: views)
        allConstraints += NSLayoutConstraint.constraints(withVisualFormat: "H:[logo(==buttons)]",
                                                         metrics: nil, views: views)
        NSLayoutConstraint.activate(allConstraints)
        
        NSLayoutConstraint.init(item: logoAnimation!,
                                attribute: .height,
                                relatedBy: .equal,
                                toItem: view,
                                attribute: .height,
                                multiplier: 0.65,
                                constant: 0).isActive = true;
        NSLayoutConstraint.init(item: buttonsStackView,
                                attribute: .top,
                                relatedBy: .equal,
                                toItem: view,
                                attribute: .centerY,
                                multiplier: 1.2,
                                constant: 0).isActive = true;
        let widthMultiplier = self.traitCollection.horizontalSizeClass == .regular ? 0.45 : 0.7
        NSLayoutConstraint.init(item: buttonsStackView,
                                attribute: .width,
                                relatedBy: .equal,
                                toItem: view,
                                attribute: .width,
                                multiplier: CGFloat(widthMultiplier),
                                constant: 0).isActive = true;
        
        
        if self.traitCollection.horizontalSizeClass == .regular {
            NSLayoutConstraint.init(item: buttonsStackView,
                                    attribute: .height,
                                    relatedBy: .equal,
                                    toItem: view,
                                    attribute: .height,
                                    multiplier: 0.2,
                                    constant: 0).isActive = true;
        }
        logoAnimation!.layoutIfNeeded()
        buttonsStackView.layoutIfNeeded()
    }
    
    // MARK: - Animations
    
    private func addLogoAnimation() {
        
        let logoAnimation = LOTAnimationView(name: "IntroScreenLogo")
        logoAnimation.translatesAutoresizingMaskIntoConstraints = false
        
        logoAnimation.contentMode = .scaleAspectFit
        
        self.view.addSubview(logoAnimation)
        self.logoAnimation = logoAnimation
    }
    
    private func animateButtons() {
        var delay = 0.0
        buttonsStackView.isHidden = false
        for button in buttonsStackView.arrangedSubviews {
            button.transform = CGAffineTransform(scaleX: 0.0, y: 0.0)
            DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
                self.setAnimationFor(button: button as! UIButton)
                button.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            }
            delay += 0.3
        }
    }
    
    private func setAnimationFor(button: UIButton) {
        let scaleAnimation = CAKeyframeAnimation(keyPath: "transform.scale.xy")
        scaleAnimation.values = [0.0, 1.0, 1.06, 0.94, 1.05, 0.96, 1.03, 0.98, 1.02, 1.0]
        scaleAnimation.keyTimes = [0.0, 0.5, 0.7, 0.9, 1.05, 1.2, 1.4, 1.7, 2.0, 2.3]
        scaleAnimation.duration = 0.5
        scaleAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        button.layer.add(scaleAnimation, forKey: "scale")
    }
    
    @objc private func introAnimationFinished() {
        let delay = 0.5
        DispatchQueue.main.asyncAfter(deadline: .now() + delay) { [weak self] in
            self?.logoAnimation!.play(fromProgress: 0.04, toProgress: 1.0, withCompletion: nil)
            self?.animateButtons()
            
            // debug
//            let storyboardName = "Alerts"
//            let bundle = Bundle.main
//            let storyboard = UIStoryboard(name: storyboardName, bundle: bundle)
//            let alert =  storyboard.instantiateViewController(withIdentifier: "NoInternet")
//            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
//                self?.present(alert, animated: true, completion: nil)
//            }
        }
    }
}

