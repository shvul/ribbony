//
//  File.swift
//  Ribbony
//
//  Created by Pavel Serdiukov on 3/26/18.
//  Copyright © 2018 shvul. All rights reserved.
//

import Foundation
import Swinject
import SwinjectStoryboard

class MainAssembler {
    var resolver: Resolver {
        return assembler.resolver
    }
    private let assembler = Assembler(container: SwinjectStoryboard.defaultContainer)
    
    init() {
        assembler.apply(assembly: StorageDownloaderAssembly())
        assembler.apply(assembly: FeedDownloaderAssembly())
        assembler.apply(assembly: FeedParserAssembly())
        assembler.apply(assembly: DataBaseManagerAssembly())
        assembler.apply(assembly: CacheControllerAssembly())
        assembler.apply(assembly: DataSyncAssembly())
        
        assembler.apply(assembly: ViewControllerLoaderAssembly())
        
        assembler.apply(assembly: IntroViewControllerAssembly())
        assembler.apply(assembly: CategoriesViewControllerAssembly())
        assembler.apply(assembly: ExersiseViewControllerAssembly())
        assembler.apply(assembly: ExersisesViewControllerAssembly())
        assembler.apply(assembly: LessonsViewControllerAssembly())
        assembler.apply(assembly: ProfileViewControllerAssembly())
        assembler.apply(assembly: PrivacyViewControllerAssembly())
    }
}
