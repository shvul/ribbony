//
//  IntroViewController.swift
//  Ribbony
//
//  Created by Pavel Serdiukov on 12/30/17.
//  Copyright © 2017 shvul. All rights reserved.
//

import UIKit
import Lottie
import Swinject
import SwinjectStoryboard

class IntroViewController: UIViewController {
    
    var dataSync: DataSync! {
        didSet {
            dataSync.delegate = self
        }
    }
    
    @IBOutlet weak var preloaderView: LiquidPreloader! {
        didSet {
            preloaderView.delegate = self
        }
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        segue.destination.transitioningDelegate = self
    }
}

// MARK: - UIViewControllerTransitioningDelegate
extension IntroViewController: UIViewControllerTransitioningDelegate {
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return CurtainTransition()
    }
    
}

// MARK: - LoaderDelegate
extension IntroViewController: LoaderDelegate {
    
    func loadingFinished() {
        self.performSegue(withIdentifier: "preloadFinished", sender: self)
    }
    
}

// MARK: - DataSuncDelegate
extension IntroViewController: DataSuncDelegate {
    func progressDidChange(to progress: Float) {
        preloaderView.updateProgress(toPercent: CGFloat(progress))
    }
    
    func syncFailed(with error: Error) {
        fatalError()
    }
}

// MARK: - DI Container
class IntroViewControllerAssembly: Assembly {
    func assemble(container: Container) {
        container.storyboardInitCompleted(IntroViewController.self) { (r, c) in
            c.dataSync = r.resolve(DataSync.self)
        }
    }
}
