//
//  FeedParser.swift
//  Ribbony
//
//  Created by Pavel Serdiukov on 7/2/18.
//  Copyright © 2018 shvul. All rights reserved.
//

import Foundation
import CoreData
import Swinject

protocol FeedParser {
    func parse(_ feed: Data) throws -> Feed?
}

class MOFeedParser: FeedParser {
    let context: NSManagedObjectContext
    
    init(with context: NSManagedObjectContext) {
        self.context = context
    }
    
    lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        return formatter
    }()
    
    func parse(_ feed: Data) throws -> Feed? {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .formatted(dateFormatter)
        decoder.userInfo = [CodingUserInfoKey(rawValue: "context")!: context]
        let result = try decoder.decode(Feed.self, from: feed)
        setupRelationships(for: result)
        return result
    }
    
    func setupRelationships(for feed: Feed) {
        for property in feed.properties {
            for lesson in property.lessons {
                lesson.property = property
                lesson.animations.forEach( {$0.lesson = lesson })
                lesson.exerciseImage.forEach( {$0.lesson = lesson })
            }
            property.propertyImage.forEach( {$0.property = property })
        }
    }
}

// MARK: - DI Container
class FeedParserAssembly: Assembly {
    func assemble(container: Container) {
        container.register(FeedParser.self) { r in
            let context = CoreDataStack.sharedInstance.persistentContainer.viewContext
            return MOFeedParser(with: context)
        }.inObjectScope(.container)
    }
}
