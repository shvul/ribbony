//
//  CategoriesViewControllerCollectionViewController.swift
//  Ribbony
//
//  Created by Pavel Serdiukov on 6/12/18.
//  Copyright © 2018 shvul. All rights reserved.
//

import UIKit
import Swinject
import SwinjectStoryboard

private let reuseIdentifier = "CaregoryCell"
private let segueIdentifier = "categoryNavigation"

class CategoriesViewController: UICollectionViewController {

    var viewControllerLoader: ViewControllerLoader!
    
    @IBAction func goHomeAction(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func showSidebarAction(_ sender: UIBarButtonItem) {
        viewControllerLoader.showSidebar(for: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let cellNib = UINib(nibName: "CategoryCell", bundle: nil)
        collectionView!.register(cellNib, forCellWithReuseIdentifier: reuseIdentifier)
    }

   
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == segueIdentifier {
            let cell = collectionView(collectionView!, cellForItemAt: sender as! IndexPath) as! CategoryCell
            let vc = segue.destination as! ExersisesViewController
            vc.title = cell.title.text
        }
    }
 

    // MARK: UICollectionViewDataSource
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return 2
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
        
        if let categoryCell = cell as? CategoryCell {
            if indexPath.row % 2 == 0 {
                categoryCell.title.text = "Ball"
                categoryCell.categoryImage.image = UIImage(named: "ball")
            } else {
                categoryCell.title.text = "Maces"
                categoryCell.categoryImage.image = UIImage(named: "mace")
            }
        }
        
        return cell
    }

    // MARK: UICollectionViewDelegate

    override func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
        guard let cell  = collectionView.cellForItem(at: indexPath) else { return }
        UIView.animate(withDuration: 0.1) {
            cell.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
        guard let cell  = collectionView.cellForItem(at: indexPath) else { return }
        UIView.animateKeyframes(withDuration: 0.5, delay: 0, options: [.calculationModeCubic], animations: {
            UIView.addKeyframe(withRelativeStartTime: 0.0, relativeDuration: 1.0/5.0, animations: {
                cell.transform = CGAffineTransform.identity.scaledBy(x: 1.02, y: 1.02)
            })
            UIView.addKeyframe(withRelativeStartTime: 1.0/5.0, relativeDuration: 2.0/5.0, animations: {
                cell.transform = CGAffineTransform.identity.scaledBy(x: 0.9, y: 0.9)
            })
            UIView.addKeyframe(withRelativeStartTime: 4.0/5.0, relativeDuration: 2.0/5.0, animations: {
                cell.transform = CGAffineTransform.identity
            })
        })
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: segueIdentifier, sender: indexPath)
    }
}

// MARK: UICollectionViewDelegateFlowLayout

extension CategoriesViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenWidth = UIScreen.main.bounds.width
        let side = screenWidth / 2 - 15
        return CGSize(width: side, height: side)
    }
}

// MARK: - DI Container
class CategoriesViewControllerAssembly: Assembly {
    func assemble(container: Container) {
        container.storyboardInitCompleted(CategoriesViewController.self) { (r, c) in
            c.viewControllerLoader = r.resolve(ViewControllerLoader.self)
        }
    }
}
