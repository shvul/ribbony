//
//  AppDelegate.swift
//  Ribbony
//
//  Created by Pavel Serdiukov on 12/16/17.
//  Copyright © 2017 shvul. All rights reserved.
//

import UIKit
import CoreData
import Alamofire
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var orientationLock = UIInterfaceOrientationMask.allButUpsideDown
    let assembler = MainAssembler()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        setupUIConstants()
//        NetworkReachabilityManager()?.startListening()
    
        print("LANGUAGE CODE: \(String(describing: Locale.current.languageCode))")
        
        FirebaseApp.configure()
        
//        let debugSettings = RemoteConfigSettings(developerModeEnabled: true)
//        RemoteConfig.remoteConfig().configSettings = debugSettings!
        
//        let ref = Storage.storage().reference(forURL: "gs://ribbony-native.appspot.com/exersises/models/girl.scn")
//        let metadata = [
//           "md5Hash": "dc04d89b405d77aa1aa659585f6b0f42"
//        ]
//
//        let newMetadata = StorageMetadata()
//        newMetadata.customMetadata = metadata
//
//        ref.updateMetadata(newMetadata) { (metadata, error) in
//            if let error = error {
//                print("Failure during metadata update. Error: \(error.localizedDescription).")
//            } else {
//                print("💕 Metadata updated! 💕")
//            }
//        }
        
    
//        let localURL = FileManager.default.urls(for: .libraryDirectory, in: .userDomainMask)[0].appendingPathComponent("exersises/thumbnails/beat@2x.png")
//        ref.write(toFile: localURL) { (url, error) in
//
//        }
        
//        let hashData = "d41d8cd98f00b204e9800998ecf8427e".data(using: .utf8)!
//        try! Xattr.set(named: "md5Hash", data: hashData, atPath: localURL.path)
        
//        let attr = try! Xattr.names(atPath: localURL.path)!
//        let attrData = try! Xattr.dataFor(named: "md5Hash", atPath: localURL.path)
//        print("😘 Attribute: \(String(data: attrData, encoding: .utf8))")
        
        assembler.resolver.resolve(DataSync.self)!.sunc()
        
        
//        RemoteConfig.remoteConfig().configSettings
        // Override point for customization after application launch.
        return true
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
//        self.saveContext()
    }
    
    func setupUIConstants() {
        UIConstants.scale = Float(UIScreen.main.scale)
    }
}

