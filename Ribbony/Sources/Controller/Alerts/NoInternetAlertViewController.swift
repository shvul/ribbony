//
//  NoInternetAlertViewController.swift
//  Ribbony
//
//  Created by Pavel Serdiukov on 1/2/18.
//  Copyright © 2018 shvul. All rights reserved.
//

import UIKit
import Lottie
import Firebase

class NoInternetAlertViewController: UIViewController {
    
    @IBOutlet weak var triangle: UIImageView!
    @IBOutlet weak var alertText: UILabel!
    @IBOutlet weak var continueOfflineBtn: UIButton!
    @IBOutlet weak var tryAgainBtn: UIButton!
    @IBOutlet weak var activity: SphereActivityIndicator!
    @IBOutlet weak var alertContainer: UIView! {
        didSet{
            let mask = CAShapeLayer()
            let rect = alertContainer.bounds
            let initialPath = UIBezierPath(rect: rect)
            mask.path = initialPath.cgPath
            alertContainer.layer.mask = mask
        }
    }
    
    private lazy var question: LOTAnimationView = { [unowned self] in
        let question = LOTAnimationView(name: "QuestionJump")
        question.translatesAutoresizingMaskIntoConstraints = false
        question.contentMode = .scaleAspectFit
        self.triangle.addSubview(question)
        return question
        }()
    
    // MARK: - Constraints
    
    @IBOutlet var tryAgainLeading: NSLayoutConstraint!
    @IBOutlet var continueBtnTrailling: NSLayoutConstraint!
    @IBOutlet var continueBtnHorSpacing: NSLayoutConstraint!
    @IBOutlet weak var textTop: NSLayoutConstraint!
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = self.view.backgroundColor?.withAlphaComponent(0.7)
        let delay = 0.5
        DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
            self.question.loopAnimation = true
            self.question.play()
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setupConstraints()
    }
    
    // MARK: - Constraints
    
    private func setupConstraints() {
        question.centerXAnchor.constraint(equalTo: triangle.centerXAnchor).isActive = true
        NSLayoutConstraint(item: question,
                           attribute: .centerY,
                           relatedBy: .equal,
                           toItem: triangle,
                           attribute: .centerY,
                           multiplier: 1.0,
                           constant: 5).isActive = true
        activity.layoutIfNeeded()
        question.layoutIfNeeded()
    }
    
    // MARK: - Actions
    
    @IBAction func tryAgainAction() {
        Analytics.logEvent(AnalyticsEvent.movedToOfflineMode.rawValue, parameters: nil)
        activity.play(animation: .spinner)
        invertAnimationConstraints()
        replaceTriangle()
    }
    
    @IBAction func continueOfflineAction() {
        Analytics.logEvent(AnalyticsEvent.movedToOfflineMode.rawValue, parameters: nil)
        dismiss(animated: true, completion: nil)
    }
}

// MARK: - CAAnimationDelegate
extension NoInternetAlertViewController: CAAnimationDelegate {
    
    func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        
        guard let id = anim.value(forKey: AnimationConstants.idKey) as? AnimationNames else { return }
        
        switch id {
        case .spinnerIntroSpinner:
            DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                self.activity.play(animation: .failure)
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 10) {
                self.invertAnimationConstraints()
                self.replaceSpinner()
            }
        case .triangleIntroSpinner, .triangleIntroTriangle, .spinnerIntroTriangle:
            print("TODO: add animation tear down action it id from: \(id.rawValue)")
        }
    }
}

// MARK: - Animations
extension NoInternetAlertViewController {
    private enum AnimationNames: String {
        case spinnerIntroTriangle = "PSSpinnerIntroTriangleAnimation"
        case spinnerIntroSpinner = "PSSpinnerIntroSpinnerAnimation"
        case triangleIntroTriangle = "PSTriangleIntroTrinangleAnimation"
        case triangleIntroSpinner = "PSTriangleIntroSpinnerAnimation"
    }
    
    private struct AnimationConstants {
        
        private init(){}
        
        static let duration = 0.5 as TimeInterval
        static let idKey = "AnimationID"
    }
    
    private func replaceTriangle() {
        activity.isHidden = false
        let centerX = triangle.center.x
        let animationName = "spinner intro"
        let path = "position.x"
        let spinnerAnimation = CAKeyframeAnimation(keyPath: path)
        let startPosition = alertContainer.frame.origin.x - activity.frame.width
        spinnerAnimation.values = [startPosition, centerX + 10, centerX - 5, centerX]
        spinnerAnimation.keyTimes = [0.0, 0.8, 0.9, 1]
        spinnerAnimation.duration = AnimationConstants.duration
        spinnerAnimation.delegate = self
        spinnerAnimation.fillMode = kCAFillModeForwards
        spinnerAnimation.isRemovedOnCompletion = false
        spinnerAnimation.setValue(AnimationNames.spinnerIntroSpinner, forKey: AnimationConstants.idKey)
        spinnerAnimation.timingFunction = CAMediaTimingFunction(controlPoints: 0.36, 0.73, 0.08, 1.0)
        activity.layer.add(spinnerAnimation, forKey: animationName)
        
        
        let triangeAnimation = CABasicAnimation(keyPath: path)
        let finalPosition = alertContainer.frame.maxX + triangle.frame.width / 2 + 10
        triangeAnimation.fromValue = centerX
        triangeAnimation.toValue = finalPosition
        triangeAnimation.duration = AnimationConstants.duration
        triangeAnimation.delegate = self
        triangeAnimation.fillMode = kCAFillModeForwards
        triangeAnimation.isRemovedOnCompletion = false
        triangeAnimation.setValue(AnimationNames.spinnerIntroTriangle, forKey: AnimationConstants.idKey)
        triangeAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        triangle.layer.add(triangeAnimation, forKey: animationName)
        
        UIView.animate(withDuration: AnimationConstants.duration, animations: {
            self.triangle.transform = CGAffineTransform(rotationAngle: CGFloat.pi / 2)
        }) { (finished) in
            self.triangle.transform = CGAffineTransform.identity
        }
    }
    
    private func replaceSpinner() {
        triangle.isHidden = false
        let centerX = triangle.center.x
        let animationName = "tringle return"
        let path = "position.x"
        let spinnerAnimation = CAKeyframeAnimation(keyPath: path)
        let finalPosition = alertContainer.frame.origin.x - activity.frame.width /*/ 2*/ - 10
//        spinnerAnimation.fromValue = centerX
//        spinnerAnimation.toValue = finalPosition
        spinnerAnimation.values = [centerX, finalPosition]
        spinnerAnimation.keyTimes = [0.3, 1.0]
        spinnerAnimation.duration = 0.5
        spinnerAnimation.delegate = self
//        spinnerAnimation.speed = 1
        spinnerAnimation.fillMode = kCAFillModeForwards
        spinnerAnimation.isRemovedOnCompletion = false
        spinnerAnimation.setValue(AnimationNames.triangleIntroSpinner, forKey: AnimationConstants.idKey)
        activity.layer.add(spinnerAnimation, forKey: animationName)
        
        UIView.animate(withDuration: AnimationConstants.duration, delay: 0.1, animations: {
            self.activity.transform = CGAffineTransform(rotationAngle: -6 * CGFloat.pi / 2)
        }) { (finished) in
            self.activity.transform = CGAffineTransform.identity
        }
        
        let triangeAnimation = CAKeyframeAnimation(keyPath: path)
        let startPosition = alertContainer.frame.maxX + triangle.frame.width / 2 + 10
        triangeAnimation.values = [startPosition, centerX - 10, centerX + 5, centerX]
        triangeAnimation.keyTimes = [0.0, 0.8, 0.9, 1.0]
        triangeAnimation.duration = 2
        triangeAnimation.delegate = self
        triangeAnimation.fillMode = kCAFillModeForwards
        triangeAnimation.isRemovedOnCompletion = false
        triangeAnimation.setValue(AnimationNames.triangleIntroTriangle, forKey: AnimationConstants.idKey)
        triangeAnimation.timingFunction = CAMediaTimingFunction(controlPoints: 0.36, 0.73, 0.08, 1.0)
        triangle.layer.add(triangeAnimation, forKey: animationName)
    }
    
    private func invertAnimationConstraints() {
        tryAgainLeading.isActive = !tryAgainLeading.isActive
        continueBtnTrailling.isActive = !continueBtnTrailling.isActive
        continueBtnHorSpacing.isActive = !continueBtnHorSpacing.isActive
        textTop.constant += tryAgainLeading.isActive ? -20 : 20
        UIView.animate(withDuration: AnimationConstants.duration - 0.2) {
            self.view.layoutIfNeeded()
        }
    }
}
