 //
//  DataBaseManager.swift
//  Ribbony
//
//  Created by Pavel Serdiukov on 6/22/18.
//  Copyright © 2018 shvul. All rights reserved.
//

import Foundation
import CoreData
import Swinject

protocol DataBaseManager {
    
    var delagate: DataBaseDelegate? { get set }
    
    func getUrls(with scale: Float) -> [Url]
    func model(named name: String) -> Model?
    func property(with title: String) -> Property?
    func lesson(with title: String) -> Lesson?
    func getFeed() -> Feed?
    func clean()
    func save()
}
protocol DataBaseDelegate {
    func actionDidFired(error: Error)
}

 class CoreDataBaseManager: DataBaseManager {
    
    var delagate: DataBaseDelegate?
    
    private let persistentContainer = CoreDataStack.sharedInstance.persistentContainer
    private let mainContext = CoreDataStack.sharedInstance.persistentContainer.viewContext
    
    func getUrls(with scale: Float) -> [Url] {
        let fetchRequest: NSFetchRequest<Url> = Url.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "scale == \(scale) OR scale == nil")
        
        guard let urls = try? mainContext.fetch(fetchRequest) else { return [] }
        return urls
    }
    
    func model(named name: String) -> Model? {
        let fetchRequest: NSFetchRequest<Model> = Model.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "name == \(name)")
        
        guard let models = try? mainContext.fetch(fetchRequest) else { return nil }
        return models.count == 1 ? models[0] : nil
    }
    
    func property(with title: String) -> Property? {
        let fetchRequest: NSFetchRequest<Property> = Property.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "title == \(title)")
        
        guard let properties = try? mainContext.fetch(fetchRequest) else { return nil }
        return properties.count == 1 ? properties[0] : nil
    }
    
    func lesson(with title: String) -> Lesson? {
        let fetchRequest: NSFetchRequest<Lesson> = Lesson.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "title == \(title)")
        
        guard let lessons = try? mainContext.fetch(fetchRequest) else { return nil }
        return lessons.count == 1 ? lessons[0] : nil
    }
    
    func getFeed() -> Feed? {
        let fetchRequest: NSFetchRequest<Feed> = Feed.fetchRequest()
        guard let feed = try? mainContext.fetch(fetchRequest) else { return nil }
        return feed.count == 1 ? feed[0] : nil
    }
    
    func clean() {
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Feed")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        do {
            try mainContext.execute(deleteRequest)
            try mainContext.save()
        } catch {
            print(error)
            delagate?.actionDidFired(error: error)
        }
    }
    
    func save() {
        do {
            try CoreDataStack.sharedInstance.saveContext()
        } catch {
            print(error)
            delagate?.actionDidFired(error: error)
        }
        
    }
    
}
 
// MARK: - DI Container
class DataBaseManagerAssembly: Assembly {
    func assemble(container: Container) {
        container.register(DataBaseManager.self) { _ in
            return CoreDataBaseManager()
        }.inObjectScope(.container)
    }
}
