//
//  Animation+CoreDataClass.swift
//  Ribbony
//
//  Created by Pavel Serdiukov on 6/26/18.
//  Copyright © 2018 shvul. All rights reserved.
//
//

import Foundation
import CoreData

@objc(Animation)
public class Animation: Url {
    private static let entityName = "Animation"
    
    required convenience public init(from decoder: Decoder) throws {
        guard let contextUserInfoKey = CodingUserInfoKey.context,
            let context = decoder.userInfo[contextUserInfoKey] as? NSManagedObjectContext,
            let entity = NSEntityDescription.entity(forEntityName: Animation.entityName, in: context) else {
                fatalError("Failed to decode Animation! Managed object context is missed or wrong entity name is set.")
        }
        self.init(entity: entity, insertInto: context)
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        model = try container.decode(String.self, forKey: .model)
        name = try container.decode(String.self, forKey: .name)
        url = try container.decode(String.self, forKey: .url)
        md5Hash = try container.decode(String.self, forKey: .md5Hash)
    }
}
extension Animation {
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<Animation> {
        return NSFetchRequest<Animation>(entityName: "Animation")
    }
    
    @NSManaged public var model: String
    @NSManaged public var name: String
    @NSManaged public var lesson: Lesson?
    
}

// MARK: Coding Keys for deserialization
extension Animation {
    private enum CodingKeys: String, CodingKey {
        case model
        case name
        case lesson
        case url
        case md5Hash
    }
}

