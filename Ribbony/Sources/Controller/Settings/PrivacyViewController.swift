//
//  PrivacyViewController.swift
//  Ribbony
//
//  Created by Pavel Serdiukov on 7/7/18.
//  Copyright © 2018 shvul. All rights reserved.
//

import UIKit
import WebKit
import FirebaseRemoteConfig
import Swinject
import SwinjectStoryboard

class PrivacyViewController: UIViewController {
    @IBOutlet weak var descWebView: WKWebView!
    
    var downloader: StorageDownloader?
    
    lazy var policyUrl: String = {
       return RemoteConfig.remoteConfig()[RCKey.privacyLink.rawValue].stringValue!
    }()
    
    lazy var localUrl: String = {
        var cachesDir = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask)[0]
        return cachesDir.appendingPathComponent("PrivacyPolicy.html").absoluteString
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupPrivacyPolicy()
    }
    
    func setupPrivacyPolicy() {
        let task = downloader!.download(from: policyUrl, to: localUrl)
        task.observe(.success, handler: { (snapshot) in
            let html = try! String(contentsOfFile: self.localUrl, encoding: String.Encoding.utf8)
            self.descWebView.loadHTMLString(html, baseURL: nil)
        })
        task.observe(.failure) { (snapshot) in
            self.descWebView.loadHTMLString(self.getLocalPrivcy(), baseURL: nil)
        }
    }
    
    func getLocalPrivcy() -> String{
        let path = Bundle.main.path(forResource: "PrivacyPolicy", ofType: "html")
        return try! String(contentsOfFile: path!, encoding: String.Encoding.utf8)
    }
    
    @IBAction func closeAction(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
}

// MARK: - DI Container
class PrivacyViewControllerAssembly: Assembly {
    func assemble(container: Container) {
        container.storyboardInitCompleted(PrivacyViewController.self) { (r, c) in
            c.downloader = r.resolve(StorageDownloader.self)
        }
    }
}
