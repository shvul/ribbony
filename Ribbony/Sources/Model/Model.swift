//
//  Model+CoreDataClass.swift
//  Ribbony
//
//  Created by Pavel Serdiukov on 6/26/18.
//  Copyright © 2018 shvul. All rights reserved.
//
//

import Foundation
import CoreData

@objc(Model)
public class Model: Url {
    private static let entityName = "Model"
    
    required convenience public init(from decoder: Decoder) throws {
        guard let contextUserInfoKey = CodingUserInfoKey.context,
            let context = decoder.userInfo[contextUserInfoKey] as? NSManagedObjectContext,
            let entity = NSEntityDescription.entity(forEntityName: Model.entityName, in: context) else {
                fatalError("Failed to decode Model! Managed object context is missed or wrong entity name is set.")
        }
        self.init(entity: entity, insertInto: context)
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        animationNode = try container.decode(String.self, forKey: .animationNode)
        name = try container.decode(String.self, forKey: .name)
        url = try container.decode(String.self, forKey: .url)
        md5Hash = try container.decode(String.self, forKey: .md5Hash)
    }
}

extension Model {
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<Model> {
        return NSFetchRequest<Model>(entityName: "Model")
    }
    
    @NSManaged public var animationNode: String
    @NSManaged public var name: String
    
}

// MARK: Coding Keys for deserialization
extension Model {
    private enum CodingKeys: String, CodingKey {
        case animationNode
        case name
        case url
        case md5Hash
    }
}
