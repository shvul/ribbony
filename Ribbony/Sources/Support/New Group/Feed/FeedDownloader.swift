//
//  FeedDownloader.swift
//  Ribbony
//
//  Created by Pavel Serdiukov on 6/21/18.
//  Copyright © 2018 shvul. All rights reserved.
//

import Foundation
import FirebaseRemoteConfig
import Swinject

typealias FeedString = String
protocol FeedDownloader {
    func fetch(complitionHandler: (@escaping (FeedString?, Error?) -> Void))
}

enum RCKey: String {
    case feed
    case bucketName
    case privacyLink
}

let defaultsPlist = "RemoteConfigDefaults"

class RemoteConfigDownloader: FeedDownloader {
    
    func fetch(complitionHandler: @escaping ((FeedString?, Error?) -> Void)) {
        setupDefaults()
//        RemoteConfig.remoteConfig().fetch(withExpirationDuration: 0) { (status, error) in
//            guard error == nil else {
//                complitionHandler(nil, error)
//                return
//            }
//            let rc = RemoteConfig.remoteConfig()
//            rc.activateFetched()
//            let feed = rc[RCKey.feed.rawValue].stringValue
//            complitionHandler(feed, nil)
//        }
        
        RemoteConfig.remoteConfig().fetch { (status, error) in
            guard error == nil else {
                complitionHandler(nil, error)
                return
            }
            let rc = RemoteConfig.remoteConfig()
            rc.activateFetched()
            let feed = rc[RCKey.feed.rawValue].stringValue
            complitionHandler(feed, nil)
        }
    }
    
    func setupDefaults() {
        RemoteConfig.remoteConfig().setDefaults(fromPlist: defaultsPlist)
    }
}

// MARK: - DI Container
class FeedDownloaderAssembly: Assembly {
    func assemble(container: Container) {
        container.register(FeedDownloader.self) { r in
            return RemoteConfigDownloader()
        }.inObjectScope(.container)
    }
}
