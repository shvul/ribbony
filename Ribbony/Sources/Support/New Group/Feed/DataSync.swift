//
//  DataSync.swift
//  Ribbony
//
//  Created by Pavel Serdiukov on 6/26/18.
//  Copyright © 2018 shvul. All rights reserved.
//

import Foundation
import Swinject

protocol DataSuncDelegate {
    func progressDidChange(to progress: Float)
    func syncFailed(with error: Error)
}
protocol DataSync {
    var delegate: DataSuncDelegate? { get set }
    func sunc()
}

class CoreDataSync: DataSync {
    
    var delegate: DataSuncDelegate?
    let feedDownloader: FeedDownloader
    let storageDownloader: StorageDownloader
    let dataManager: DataBaseManager
    let feedParser: FeedParser
    let cacheController: CacheController
    
    init(feedDownloader: FeedDownloader, storageDownloader: StorageDownloader, dataManager: DataBaseManager, feedParser: FeedParser, cacheController: CacheController) {
        self.feedDownloader = feedDownloader
        self.storageDownloader = storageDownloader
        self.dataManager = dataManager
        self.feedParser = feedParser
        self.cacheController = cacheController
    }
    
    func sunc() {
        feedDownloader.fetch { [unowned self] (feed, error) in
            guard error == nil else {
                self.delegate?.syncFailed(with: error!)
                return
            }
            if let feed = feed {
                self.dataManager.clean()
                // REMOVE: debugging cache
//                self.cacheController.clean()
                self.updateStorage(feed)
                self.downloadFiles()
            }
        }
    }
    
    func updateStorage(_ feed: String) {
        do {
            let data = feed.data(using: .utf8)!
            _ = try feedParser.parse(data)
            dataManager.save()
        } catch {
            self.delegate?.syncFailed(with: error)
        }
    }
    
    var observer: NSKeyValueObservation?
    
    func downloadFiles() {
        let urls = dataManager.getUrls(with: UIConstants.scale).filter { !cacheController.isObjectCached($0) }
        
        if urls.isEmpty {
            self.delegate?.progressDidChange(to: 1)
            return
        } else {
            cacheController.cleanTrash()
        }
        
        let progress = Progress(totalUnitCount: Int64(urls.count))
        observer = progress.observe(\.completedUnitCount) { (progress, change) in
            self.delegate?.progressDidChange(to: Float(progress.fractionCompleted))
        }
        for url in urls {
            let task = storageDownloader.download(file: url)
            var failureCounter = 0;
            task.observe(.failure) { (snapshot) in
                if let error = snapshot.error {
                    print("Error during file downloading: \(error.localizedDescription)")
                    failureCounter += 1
                }
                if failureCounter == 0 {
                    task.enqueue()
                } else {
                    self.delegate?.syncFailed(with: snapshot.error!)
                }
            }
            
            task.observe(.success) { (snapshot) in
                progress.completedUnitCount += 1
                self.cacheController.cache(object: url)
            }
        }
    }
}

// MARK: - DataBaseDelegate
extension CoreDataSync: DataBaseDelegate {
    
    func actionDidFired(error: Error) {
        delegate?.syncFailed(with: error)
    }
}

// MARK: - DI Container
class DataSyncAssembly: Assembly {
    func assemble(container: Container) {
        container.register(DataSync.self) { r in
            let feedDownloader = r.resolve(FeedDownloader.self)!
            let storageDownloader = r.resolve(StorageDownloader.self)!
            let dataManager = r.resolve(DataBaseManager.self)!
            let feedParser = r.resolve(FeedParser.self)!
            let cacheController = r.resolve(CacheController.self)!
            return CoreDataSync(feedDownloader: feedDownloader,
                                storageDownloader: storageDownloader,
                                dataManager: dataManager,
                                feedParser: feedParser,
                                cacheController: cacheController)
        }.inObjectScope(.container)
    }
}

