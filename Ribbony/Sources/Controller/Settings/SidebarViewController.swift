//
//  SidebarViewController.swift
//  Ribbony
//
//  Created by Pavel Serdiukov on 7/6/18.
//  Copyright © 2018 shvul. All rights reserved.
//

import UIKit

class SidebarViewController: UIViewController {
    
    var shouldBeDismissed = false
    let settingsSegueIdentifier = "showSettings"
    
    @IBOutlet weak var closeBtn: UIButton!
    
    @IBOutlet weak var shadowView: UIView!
    
    @IBOutlet weak var sidebarContainerView: UIView!
    
    @IBOutlet weak var containerViewTrailingConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var profileStackView: UIStackView! {
        didSet {
            let recognizer = UITapGestureRecognizer(target: self, action: #selector(self.showProfile))
            profileStackView.addGestureRecognizer(recognizer)
        }
    }
    
    @IBOutlet weak var unlockMoreStackView: UIStackView! { 
        didSet {
            let recognizer = UITapGestureRecognizer(target: self, action: #selector(self.showSubscription))
            unlockMoreStackView.addGestureRecognizer(recognizer)
        }
    }
    
    @IBOutlet weak var settingsStackView: UIStackView! {
        didSet {
            let recognizer = UITapGestureRecognizer(target: self, action: #selector(self.showSettings))
            settingsStackView.addGestureRecognizer(recognizer)
        }
    }
    
    @IBOutlet weak var privacyStackView: UIStackView! {
        didSet {
            let recognizer = UITapGestureRecognizer(target: self, action: #selector(self.showPrivacy))
            privacyStackView.addGestureRecognizer(recognizer)
        }
    }
    
    @IBOutlet weak var faqStackView: UIStackView! {
        didSet {
            let recognizer = UITapGestureRecognizer(target: self, action: #selector(self.showFAQ))
            faqStackView.addGestureRecognizer(recognizer)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.closeAction(_:)))
        shadowView.addGestureRecognizer(tap)
        shadowView.isUserInteractionEnabled = true
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.closeAction(_:)))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        view.addGestureRecognizer(swipeRight)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if shouldBeDismissed {
            dismiss(animated: false, completion: nil)
        }
        let width = sidebarContainerView.frame.width
        sidebarContainerView.transform = sidebarContainerView.transform.translatedBy(x: width, y: 0)
        let initialAlpha = shadowView.alpha
        shadowView.alpha = 0
        UIView.animate(withDuration: 0.3) {
            self.sidebarContainerView.transform = CGAffineTransform.identity
            self.shadowView.alpha = initialAlpha
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        let width = sidebarContainerView.frame.width
        let finalTransform = self.sidebarContainerView.transform.translatedBy(x: width, y: 0)
        
        UIView.animate(withDuration: 0.3, animations: {
            self.sidebarContainerView.transform = finalTransform
            self.shadowView.alpha = 0
        }) { (result) in
            self.sidebarContainerView.isHidden = true
        }
    }
    
    @IBAction func closeAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
    // MARK: - Navigation
    @objc func showProfile() {
        shouldBeDismissed = true
        performSegue(withIdentifier: settingsSegueIdentifier, sender: 0)
    }
    
    @objc func showSubscription() {
        shouldBeDismissed = true
        performSegue(withIdentifier: settingsSegueIdentifier, sender: 1)
    }
    
    @objc func showSettings() {
        shouldBeDismissed = true
        performSegue(withIdentifier: settingsSegueIdentifier, sender: 2)
    }
    
    @objc func showPrivacy() {
        shouldBeDismissed = true
        performSegue(withIdentifier: settingsSegueIdentifier, sender: 3)
    }
    
    @objc func showFAQ() {
        shouldBeDismissed = true
        performSegue(withIdentifier: settingsSegueIdentifier, sender: 4)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let tabBarController = segue.destination as? TabBarViewController {
            tabBarController.selectedIndex = sender as! Int
            
        }
    }

}
