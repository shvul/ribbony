//
//  AnalyticConstants.swift
//  Ribbony
//
//  Created by Pavel Serdiukov on 4/4/18.
//  Copyright © 2018 shvul. All rights reserved.
//

import Foundation

enum AnalyticsEvent: String {
    case movedToOfflineMode = "moved_to_offline_mode"
    case tryToLoadFeedAgain = "try_to_load_feed_again"
}
