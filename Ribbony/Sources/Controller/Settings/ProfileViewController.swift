//
//  ProfileViewController.swift
//  Ribbony
//
//  Created by Pavel Serdiukov on 7/8/18.
//  Copyright © 2018 shvul. All rights reserved.
//

import UIKit
import Lottie
import SwinjectStoryboard
import Swinject

class ProfileViewController: UIViewController {

    var viewControllerLoader: ViewControllerLoader!
    
    private lazy var builder: LOTAnimationView = { [unowned self] in
        let builder = LOTAnimationView(name: "Builder")
        builder.loopAnimation = true
        builder.translatesAutoresizingMaskIntoConstraints = false
        builder.contentMode = .scaleAspectFit
        return builder
        }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(builder)

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        builder.play()
    }
    
    @IBAction func closeAction(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        NSLayoutConstraint.activate([
            builder.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            builder.bottomAnchor.constraint(equalTo: tabBarController!.tabBar.topAnchor),
            ])
        
        var contraints = [NSLayoutConstraint]()
        contraints.append(NSLayoutConstraint(item: builder,
                                             attribute: .height,
                                             relatedBy: .equal,
                                             toItem: builder,
                                             attribute: .width,
                                             multiplier: 9.0 / 5.0,
                                             constant: 0))
        contraints.append(NSLayoutConstraint(item: builder,
                                             attribute: .height,
                                             relatedBy: .equal,
                                             toItem: view,
                                             attribute: .height,
                                             multiplier: 0.3,
                                             constant: 0))
        
        contraints.append(builder.leadingAnchor.constraint(equalTo: view.leadingAnchor))
        NSLayoutConstraint.activate(contraints)
        view.layoutIfNeeded()
    }

}

// MARK: - DI Container
class ProfileViewControllerAssembly: Assembly {
    func assemble(container: Container) {
        container.storyboardInitCompleted(ProfileViewController.self) { (r, c) in
            c.viewControllerLoader = r.resolve(ViewControllerLoader.self)
        }
    }
}
