//
//  StorageDownloader.swift
//  Ribbony
//
//  Created by Pavel Serdiukov on 7/3/18.
//  Copyright © 2018 shvul. All rights reserved.
//

import Foundation
import FirebaseStorage
import Swinject

protocol StorageDownloader {
    func download(file: DownloadableFile) -> StorageDownloadTask
    func download(from url: String, to localUrl: String) -> StorageDownloadTask
}
protocol DownloadableFile: Cachable {
    var url: String { get set }
}
class GoogleStorageDonwloader: StorageDownloader {
    func download(file: DownloadableFile) -> StorageDownloadTask {
        return download(from: file.url, to: file.localUrl.path)
    }
    
    func download(from url: String, to localUrl: String) -> StorageDownloadTask {
        let ref = Storage.storage().reference(forURL: url)
        let downloadTask = ref.write(toFile: URL(fileURLWithPath: localUrl))
        return downloadTask
    }
}

// MARK: - DI Container
class StorageDownloaderAssembly: Assembly {
    func assemble(container: Container) {
        container.register(StorageDownloader.self) { r in
            return GoogleStorageDonwloader()
        }.inObjectScope(.container)
    }
}
